package com.rodrigo.projectmanager.controllers;

import com.rodrigo.projectmanager.mappings.response.WeekResponse;
import com.rodrigo.projectmanager.service.week.WeekService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WeeksController {

    private final WeekService weekService;

    @Autowired
    public WeeksController(WeekService weekService) {
        this.weekService = weekService;
    }

    @GetMapping("/week")
    public ResponseEntity<List<WeekResponse>> getWeekDays() {

        return ResponseEntity.ok(weekService.getWeekDays());
    }

}
