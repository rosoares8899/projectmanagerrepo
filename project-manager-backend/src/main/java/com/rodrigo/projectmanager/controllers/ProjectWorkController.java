package com.rodrigo.projectmanager.controllers;

import com.rodrigo.projectmanager.mappings.request.StartWorkRequest;
import com.rodrigo.projectmanager.models.ProjectWork;
import com.rodrigo.projectmanager.service.projectwork.ProjectWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/project-work")
public class ProjectWorkController {

    private final ProjectWorkService service;

    @Autowired
    public ProjectWorkController(ProjectWorkService service) {

        this.service = service;
    }

    @GetMapping("")
    public List<ProjectWork> all() {
        return service.findAll();
    }

    @GetMapping("by-date/{date}")
    public List<ProjectWork> byDate(@PathVariable String date) {

        return service.findByDate(date);
    }

    @PostMapping("")
    public ResponseEntity work(@RequestBody StartWorkRequest workRequest) {

        try {
            service.work(workRequest);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body(e);
        }

        return ResponseEntity.created(URI.create("project-work")).build();
    }

    @PutMapping("stop-work/{projectWorkId}")
    public ResponseEntity stopWork(@PathVariable Long projectWorkId) {

        try {
            return ResponseEntity.ok(service.stopWork(projectWorkId));
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body(e);
        }
    }

}
