package com.rodrigo.projectmanager.service.projectwork.impl;

import com.rodrigo.projectmanager.mappings.request.StartWorkRequest;
import com.rodrigo.projectmanager.models.Project;
import com.rodrigo.projectmanager.models.ProjectWork;
import com.rodrigo.projectmanager.repository.ProjectWorkRepository;
import com.rodrigo.projectmanager.service.project.ProjectService;
import com.rodrigo.projectmanager.service.projectwork.ProjectWorkService;
import com.rodrigo.projectmanager.service.registerwork.RegisterWorkInFile;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectWorkServiceImpl implements ProjectWorkService {

    private final ProjectWorkRepository repository;
    private final ProjectService projectService;
    private final RegisterWorkInFile registerWorkInFile;

    @Autowired
    public ProjectWorkServiceImpl(ProjectWorkRepository repository, ProjectService projectService, RegisterWorkInFile registerWorkInFile) {
        this.repository = repository;
        this.projectService = projectService;
        this.registerWorkInFile = registerWorkInFile;
    }

    @Override
    public ProjectWork work(StartWorkRequest workRequest) throws Exception {

        ProjectWork work = new ProjectWork();
        work.setDayOfWork(workRequest.getDate());
        work.setWorkDescription(workRequest.getDescription());
        work.setHourStart(workRequest.getHourStart() != null ? workRequest.getHourStart() : LocalTime.now());
        work.setCurrentWorking(true);

        Optional<Project> project = projectService.findById(workRequest.getProject());

        if (project.isEmpty()) {
            throw new Exception("Projeto não está cadastrado");
        }

        work.setProject(project.get());

        return repository.save(work);
    }

    @Override
    public ProjectWork stopWork(Long projectId) throws Exception {

        Optional<ProjectWork> optional = repository.findById(projectId);

        if (optional.isEmpty()) {
            throw new Exception("Trabalho corrente para projeto: " + projectId + " não econtrado");
        }

        ProjectWork projectWork = optional.get();

        projectWork.setCurrentWorking(false);
        projectWork.setHourEnd(LocalTime.now().minusMinutes(1));

        repository.save(projectWork);

        registerWorkInFile.register(projectWork);

        return projectWork;
    }

    @Override
    public List<ProjectWork> findAll() {
        return repository.findAll();
    }

    @Override
    public List<ProjectWork> findByDate(String date) {

        LocalDate localDate = LocalDate.parse(date);

        return repository.findByDayOfWork(localDate);
    }
}
