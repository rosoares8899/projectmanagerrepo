package com.rodrigo.projectmanager.service.project;

import com.rodrigo.projectmanager.models.Project;

import java.util.Optional;

public interface ProjectService {

    Project findByCode(int code);

    Optional<Project> findById(Long id);

}
