import axios from "axios";

const http = axios.create({
  baseURL: 'http://localhost:6868',
});

export default http;