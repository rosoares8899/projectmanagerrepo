package com.rodrigo.projectmanager;

import com.rodrigo.projectmanager.models.Project;
import com.rodrigo.projectmanager.models.ProjectWork;
import com.rodrigo.projectmanager.service.registerwork.GoogleSheetsRegisterWork;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.Random;

public class GoogleSheetsRegisterWorkTest {

    private static final String testSpreadsheetId = "1EeG5F2l6YZMXnsr4eLOz8M29zt4NveaDieeaArP4neU";

    @Test
    public void testItCanRegisterAProjectWorkAtTestSheet() throws Exception {

        ProjectWork projectWork = createProjectWork();

        GoogleSheetsRegisterWork googleSheetsRegisterWork = new GoogleSheetsRegisterWork();

        googleSheetsRegisterWork.setSpreadSheetId(testSpreadsheetId);

        boolean registered = googleSheetsRegisterWork.register(projectWork);

        Assert.isTrue(registered, "Não foi atualizada nenhuma linha");

    }

    private ProjectWork createProjectWork() {

        ProjectWork projectWork = new ProjectWork();

        projectWork.setProject(createProject());
        projectWork.setDayOfWork(LocalDate.now());
        projectWork.setHourStart(LocalTime.of(9, 30));
        projectWork.setHourEnd(LocalTime.now());
        projectWork.setWorkDescription("Descrição Teste");

        return projectWork;

    }

    private Project createProject() {

        Project project = new Project();
        Random random = new Random();

        project.setCode(random.nextInt(10000));

        return project;
    }

}
