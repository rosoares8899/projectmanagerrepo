package com.rodrigo.projectmanager.repository;

import com.rodrigo.projectmanager.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findProjectByCode(int code);

}
