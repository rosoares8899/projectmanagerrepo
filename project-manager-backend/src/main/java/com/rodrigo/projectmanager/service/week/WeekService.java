package com.rodrigo.projectmanager.service.week;

import com.rodrigo.projectmanager.mappings.response.WeekResponse;

import java.util.List;

public interface WeekService {

    List<WeekResponse> getWeekDays();

}
