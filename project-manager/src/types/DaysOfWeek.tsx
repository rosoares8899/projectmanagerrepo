export default interface DaysOfWeek{
    day: string,
    date: string,
    dateString: string,
    today: boolean
}