package com.rodrigo.projectmanager.mappings.response;

import java.time.LocalDate;

public class WeekResponse {

    private String day;

    private LocalDate date;

    private String dateString;

    private boolean today;

    public WeekResponse(String day, LocalDate date, String dateString, boolean today) {
        this.day = day;
        this.date = date;
        this.dateString = dateString;
        this.today = today;
    }

    public WeekResponse() {
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public boolean isToday() {
        return today;
    }

    public void setToday(boolean today) {
        this.today = today;
    }
}
