package com.rodrigo.projectmanager.controllers;

import com.rodrigo.projectmanager.models.Project;
import com.rodrigo.projectmanager.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
public class ProjectController {

    private final ProjectRepository repository;

    @Autowired
    public ProjectController(ProjectRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/projects")
    public List<Project> list() {
        return repository.findAll();
    }

    @PostMapping("/projects")
    public ResponseEntity<Project> create(@RequestBody Project project) {

        Project saved = repository.save(project);

        return ResponseEntity.created(URI.create("project" + saved.getId())).build();
    }


}
