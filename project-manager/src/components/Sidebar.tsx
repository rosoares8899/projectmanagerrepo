import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/sidebars.css';
import {useState} from "react";
import Home from "./Home";

function Sidebar() {

  const [component, setComponent] = useState('home');

  return (
    <div className="d-flex flex-row">
      <div className="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark" style={{width: 280, height: 880}}>
        <a href="/" className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
          <span className="fs-4">Projetos Morpheus</span>
        </a>
        <hr/>
        <ul className="nav nav-pills flex-column mb-auto">
          <li className="nav-item">
            <a className="nav-link text-white" aria-current="page" onClick={() => setComponent('home')}
               style={{cursor: 'pointer'}}>
              Início
            </a>
          </li>
          <li>
            <a className="nav-link text-white" onClick={() => setComponent('projetos')} style={{cursor: 'pointer'}}>
              Projetos
            </a>
          </li>
        </ul>
      </div>
      <div className="p-3" style={{ width: '-webkit-fill-available' }}>
        <Home/>
      </div>
    </div>
  )
}

export default Sidebar;