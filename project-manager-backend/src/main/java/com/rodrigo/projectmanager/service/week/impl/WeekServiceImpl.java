package com.rodrigo.projectmanager.service.week.impl;

import com.rodrigo.projectmanager.mappings.response.WeekResponse;
import com.rodrigo.projectmanager.service.week.WeekService;
import com.rodrigo.projectmanager.utils.DateTimeUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class WeekServiceImpl implements WeekService {

    @Override
    public List<WeekResponse> getWeekDays() {

        List<LocalDate> datesOfWeek = DateTimeUtils.datesOfWeekDate(LocalDate.now(ZoneId.of("America/Sao_Paulo")));

        return getWeekResponse(datesOfWeek);
    }

    private List<WeekResponse> getWeekResponse(List<LocalDate> datesOfWeek) {

        List<WeekResponse> response = new ArrayList<>();

        datesOfWeek.forEach(weekDate -> {

            WeekResponse weekResponse = new WeekResponse();
            weekResponse.setDay(weekDate.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH));
            weekResponse.setDate(weekDate);
            weekResponse.setDateString(weekDate.format(DateTimeFormatter.ofPattern("EEE, dd MMM uuuu")));
            weekResponse.setToday(DateTimeUtils.dateIsToday(weekDate));

            response.add(weekResponse);
        });

        return response;
    }
}
