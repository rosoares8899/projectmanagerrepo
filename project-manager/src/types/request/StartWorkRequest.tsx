export default interface StartWorkRequest {
    project: number|null,
    description: string|null,
    date: string|null|undefined,
    hourStart: string|null
};