import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/js/bootstrap.esm';
import 'bootstrap/dist/js/bootstrap';
import React, { useState, useEffect } from 'react';
import DaysOfWeek from '../types/DaysOfWeek';
import classNames from 'classnames';
import {BsFillClockFill, BsPlusLg, BsStopCircle} from 'react-icons/bs';
import Project from '../types/Project';
import http from '../services/http'; 
import { AxiosResponse } from 'axios';
import StartWorkRequest from '../types/request/StartWorkRequest';
import ProjectWork from '../types/ProjectWork';

function getDaysOfWeek():Promise<AxiosResponse<DaysOfWeek[]>> {

  return http.get<DaysOfWeek[]>('/week', {
    headers:{
      'Access-Control-Allow-Origin':'*',
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });

}

function getProjectList():Promise<AxiosResponse<Project[]>> {
  
  return http.get<Project[]>('/projects', {
    headers:{
      'Access-Control-Allow-Origin':'*',
      'Content-Type': 'application/json;charset=UTF-8',
    }
  });

}

function getWorksOfDayActive(date:string|undefined):Promise<AxiosResponse<ProjectWork[]>> {
  return http.get<ProjectWork[]>('/project-work/by-date/'+date);
}

function Home() {

  const [project, setProject] = useState<number|null>(null);
  const [description, setDescription] = useState<string|null>(null);
  const [startTime, setStartTime] = useState<string|null>(null);
  const [daysOfWeek, setDaysOfWeek] = useState<Array<DaysOfWeek>>();
  const [projects, setProjects] = useState<Array<Project>>();
  const [dayActive, setDayActive] = useState<DaysOfWeek>();
  const [startWorkSuccess, setStartWorkSuccess] = useState<boolean>();
  const [worksOfDayActive, setWorksOfDayActive] = useState<Array<ProjectWork>>();
  const [isWorkingOnSomething, setIsWorkingOnSomething] = useState<boolean>(false);


  useEffect(() => {
    
    getDaysOfWeek().then(response => {
      setDaysOfWeek(response.data);
      setDayActive(response.data.find(day => day.today))
    }).catch(error => {
      alert('Erro ao buscar dias da semana'+error);
    });

    getProjectList().then(response => {
      setProjects(response.data);
    }).catch(error => {
      alert('Erro ao buscar projetos'+error);
    })

  }, [])

  useEffect(() => {

    if(dayActive !== undefined) {
      fetchWorksOfDay();
    }

  }, [dayActive])

  function fetchWorksOfDay() {
    getWorksOfDayActive(dayActive?.date).then(response => {
      setWorksOfDayActive(response.data);
      setIsWorkingOnSomething(response.data.some(work => work.currentWorking === true));
    })
  }

  function startWorkAgain(projectWork:ProjectWork) {

    const data:StartWorkRequest = {
      project: projectWork.project.id,
      description: projectWork.workDescription,
      date: dayActive?.date,
      hourStart: null
    };

    startWork(data);

  }

  function startWork(request:StartWorkRequest|null) {

    let data:StartWorkRequest|null = request;

    if(request === null) {
      data = {
        project: project,
        description: description,
        date: dayActive?.date,
        hourStart: startTime
      };
    }

    http.post('/project-work', data).then(response => {
      if(response.status === 201) {
        alert("Iniciado com sucesso");
        fetchWorksOfDay();
        setStartWorkSuccess(true);
      }
    }).catch(() => {
      alert("Erro ao iniciar");
      setStartWorkSuccess(false);
    })
  }

  function stopWork(projectWorkId:number) {
    http.put('/project-work/stop-work/'+projectWorkId).then(response => {
      if(response.status === 200) {
        alert("Trabalho finalizado");
        setIsWorkingOnSomething(false);
        fetchWorksOfDay();
      }
    }).catch((error) => {
      alert("Ocorreu um erro ao finalizar trabalho "+error);
      fetchWorksOfDay();
    })
  }

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <h5>{ dayActive?.today ? <b>Today: </b> : "" }{ dayActive?.dateString }</h5>
        </div>
        <div className="row mt-2">
          <div className="col-12">
            <div className="d-flex">
              <div className="me-4">
                <button className="btn btn-lg btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
                  <BsPlusLg />
                  </button>
              </div>
              <div className="btn-group btn-group-lg" role="group" aria-label="Basic outlined example">
                {
                  daysOfWeek?.map((dayOfWeek) => {
                    return (
                      <button 
                        key={dayOfWeek.day} 
                        type="button" 
                        className={classNames({
                          'btn btn-outline-primary': true,
                          'active': dayOfWeek.day === dayActive?.day
                        })} 
                        onClick={() => setDayActive(dayOfWeek)}
                      >
                        {dayOfWeek.day}
                      </button>
                    )
                  })
                }
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-12">
            <table className="table table-borderless">
              <thead>
                <tr>
                  <th scope="col">Code</th>
                  <th scope="col">Project</th>
                  <th scope="col">Description</th>
                  <th scope="col">Start</th>
                  <th scope="col">End</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {
                  worksOfDayActive?.map(work => {
                    return (
                      <tr key={work.id}>
                        <td>{work.project.code}</td>
                        <td>{work.project.name}</td>
                        <td>{work.workDescription}</td>
                        <td>{work.hourStart}</td>
                        <td>{work.hourEnd}</td>
                        <td>
                          {
                            work.currentWorking ? 
                              (
                                <button type="button" className="btn btn-dark" onClick={() => stopWork(work.id)}>
                                  <BsStopCircle />
                                  <span className="ms-2">Stop</span>
                                </button>
                              ) :
                              (
                                <>
                                  {
                                    isWorkingOnSomething || !dayActive?.today ?
                                    (
                                      <button type="button" className="btn btn-primary" disabled>
                                        <BsFillClockFill/>
                                        <span className="ms-2">Start</span>
                                      </button>
                                    ) : 
                                    (
                                      <button type="button" className="btn btn-primary" onClick={() => startWorkAgain(work)}>
                                        <BsFillClockFill/>
                                        <span className="ms-2">Start</span>
                                      </button>
                                    )
                                  }
                                </>
                              )
                          }
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          </div>
        </div>    
      </div>
      <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">New entry for: {dayActive?.dateString}</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"/>
            </div>
            <div className="modal-body">
              <div className="row g-3">
                <div className="col-12">
                  <label htmlFor="project">Project / Task</label>  
                  <select 
                    className="form-select form-select-lg" 
                    id="project" 
                    aria-label=".form-select-lg example" 
                    onChange={(e :React.ChangeEvent<HTMLSelectElement>) => setProject(parseInt(e.target.value))}
                  >
                    <option value="">Choose a project</option>
                    {
                      projects?.map((project) => {
                        return (
                          <option key={project.id} value={project.id}>{project.name}</option>
                        )
                      })
                    }
                  </select>
                </div>
              </div>
              <div className="row g-3 pt-3">
                <div className="col-6">
                  <label htmlFor="description">Description</label>
                  <textarea 
                    className="form-control" 
                    id="description" 
                    rows={2} 
                    cols={26}
                    onChange={(e:React.ChangeEvent<HTMLTextAreaElement>) => setDescription(e.target.value)}
                  />
                </div>
                <div className="col-6">
                  <label htmlFor="startTime">Start Time</label>
                  <input 
                    type="time" 
                    className="form-control form-control-lg" 
                    id="startTime"
                    onChange={(e:React.ChangeEvent<HTMLInputElement>) => setStartTime(e.target.value)}
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={() => startWork(null)}>Salvar e Iniciar</button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Home;