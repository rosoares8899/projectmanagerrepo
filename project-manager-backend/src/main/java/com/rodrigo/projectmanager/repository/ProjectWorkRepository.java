package com.rodrigo.projectmanager.repository;

import com.rodrigo.projectmanager.models.Project;
import com.rodrigo.projectmanager.models.ProjectWork;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ProjectWorkRepository extends JpaRepository<ProjectWork, Long> {

    ProjectWork findByProjectIdAndCurrentWorkingIsTrue(String projectId);

    List<ProjectWork> findByProject(Project project);

    List<ProjectWork> findByDayOfWork(LocalDate date);

}
