import Project from "./Project";

export default interface ProjectWork {
    id: number,
    dayOfWork: string,
    hourStart: string,
    hourEnd: string|null,
    workDescription: string,
    project: Project,
    currentWorking: boolean  
}