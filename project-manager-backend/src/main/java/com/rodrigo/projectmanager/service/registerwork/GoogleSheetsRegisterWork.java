package com.rodrigo.projectmanager.service.registerwork;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.rodrigo.projectmanager.models.ProjectWork;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.rodrigo.projectmanager.utils.DateTimeUtils.formatDate;
import static com.rodrigo.projectmanager.utils.DateTimeUtils.formatHour;

@Service
public class GoogleSheetsRegisterWork implements RegisterWorkInFile {

    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);

    private static final String CREDENTIALS_FILE_PATH = "/project-manager-330521-f7b3513a3ac9.json";

    private String spreadSheetId = "1SO8VWkzxW4GT3SCDy1Z0XKlraxULae0KS942p4pJ_4Q";

    @Override
    public boolean register(ProjectWork projectWork) throws Exception {

        AppendValuesResponse response = appendSpreadSheetValue("Teste!A1:E", convertProjectWorkToSpreadsheetValueRange(projectWork));

        return response.getUpdates().getUpdatedRows() >= 1;

    }

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = GoogleSheetsRegisterWork.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
          HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
          .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
          .build();

        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }

    private static GoogleCredential getDeprecatedGoogleCredentials() throws IOException {
        return GoogleCredential.fromStream(GoogleSheetsRegisterWork.class.getResourceAsStream(CREDENTIALS_FILE_PATH))
          .createScoped(SCOPES);
    }

    private static Sheets getSheetService() throws IOException, GeneralSecurityException {

        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        return new Sheets.Builder(httpTransport, JSON_FACTORY, getDeprecatedGoogleCredentials())
          .setApplicationName(APPLICATION_NAME)
          .build();
    }

    private ValueRange convertProjectWorkToSpreadsheetValueRange(ProjectWork projectWork) {

        List<List<Object>> values = Arrays.asList(
          Arrays.asList(
            projectWork.getProject().getCode(),
            projectWork.getDayOfWork().format(DateTimeFormatter.ofPattern("d/MM/uuuu")),
            formatHour(projectWork.getHourStart()),
            formatHour(projectWork.getHourEnd()),
            projectWork.getWorkDescription()
          )
        );

        return new ValueRange().setValues(values);
    }

    private AppendValuesResponse appendSpreadSheetValue(String range, ValueRange body)
      throws GeneralSecurityException, IOException {

        return getSheetService().spreadsheets().values()
          .append(spreadSheetId, range, body)
          .setValueInputOption("USER_ENTERED")
          .execute();

    }

    public String getSpreadSheetId() {
        return spreadSheetId;
    }

    public void setSpreadSheetId(String spreadSheetId) {
        this.spreadSheetId = spreadSheetId;
    }
}
