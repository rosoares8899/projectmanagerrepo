package com.rodrigo.projectmanager.service.projectwork;

import com.rodrigo.projectmanager.mappings.request.StartWorkRequest;
import com.rodrigo.projectmanager.models.ProjectWork;

import java.time.LocalDate;
import java.util.List;

public interface ProjectWorkService {

    ProjectWork work(StartWorkRequest workRequest) throws Exception;

    ProjectWork stopWork(Long projectId) throws Exception;

    List<ProjectWork> findAll();

    List<ProjectWork> findByDate(String date);

}
