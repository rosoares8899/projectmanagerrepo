package com.rodrigo.projectmanager.utils;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DateTimeUtils {

    public static String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        return formatter.format(date);
    }

    public static String formatHour(LocalTime hour) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        return hour.format(formatter);
    }

    public static List<LocalDate> datesOfWeekDate(LocalDate date) {
        LocalDate monday = date.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));

        return IntStream.range(0, 7).mapToObj(monday::plusDays).collect(Collectors.toList());
    }

    public static boolean dateIsToday(LocalDate date) {
        if (date.isBefore(LocalDate.now(ZoneId.of("America/Sao_Paulo")))) {
            return false;
        }

        return !date.isAfter(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
    }

}
