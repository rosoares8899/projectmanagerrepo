package com.rodrigo.projectmanager.service.registerwork;

import com.rodrigo.projectmanager.models.ProjectWork;

public interface RegisterWorkInFile {

    boolean register(ProjectWork projectWork) throws Exception;

}
