package com.rodrigo.projectmanager.service.project.impl;

import com.rodrigo.projectmanager.models.Project;
import com.rodrigo.projectmanager.repository.ProjectRepository;
import com.rodrigo.projectmanager.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProjectServiceInterfaceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceInterfaceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project findByCode(int code) {
        return projectRepository.findProjectByCode(code);
    }

    @Override
    public Optional<Project> findById(Long id) {
        return projectRepository.findById(id);
    }
}
