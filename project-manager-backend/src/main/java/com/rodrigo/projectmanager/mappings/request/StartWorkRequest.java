package com.rodrigo.projectmanager.mappings.request;

import com.rodrigo.projectmanager.models.Project;

import java.time.LocalDate;
import java.time.LocalTime;

public class StartWorkRequest {

    private Long project;

    private String description;

    private LocalDate date;

    private LocalTime hourStart;

    public StartWorkRequest(Long project, String description, LocalDate date, LocalTime hourStart) {
        this.project = project;
        this.description = description;
        this.date = date;
        this.hourStart = hourStart;
    }

    public Long getProject() {
        return project;
    }

    public void setProject(Long project) {
        this.project = project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getHourStart() {
        return hourStart;
    }

    public void setHourStart(LocalTime hourStart) {
        this.hourStart = hourStart;
    }
}
